"use strict";

	var total_earn = $("#overtime-earn");
	Chart.defaults.scale.ticks.beginAtZero = true;

	var barchart = new Chart(total_earn, {
		type: 'line',
		data: {
			labels:["Nov'18","Dec'18","Jan'19","Feb'19","Mar'19","Apr'19"],
			datasets: [
				{
					fill: false,
					borderColor: '#8890f9',
					label: 'AMOUNT EARNED (USD)',
					data: [0,0.4,0.15,0.5,0.19,0.6]
				}
			]
		},

		options: {
			cutoutPercentage: 10,
			animation: {
				animateScale: true
			}
		}
	});

	var year = $("#bid-chart");

	var barchart = new Chart(year, {
		type: 'doughnut',
		data: {
			labels: ["BIDDING REMAINING","UNEARNED BIDS"],
			datasets: [
				{
					label: "Point",
					backgroundColor: ["#189fee",'#ededed'],
					data: [20]
				}
			]
		},

		options: {
			cutoutPercentage: 70,
			rotation: 1 * Math.PI,
    		circumference: 1 * Math.PI,
			animation: {
				animateScale: true
			}
		}
	});


	var bid = $("#stacked-chart");
	Chart.defaults.scale.ticks.beginAtZero = true;

	var barchart = new Chart(bid, {
		type: 'bar',
		data: {
			labels:["Nov'18","Dec'18","Jan'19","Feb'19","Mar'19","Apr'19"],
			datasets: [
				{
					label: 'BIDS AWARDS TO YOU',
					data: [10,23,23],
					backgroundColor: '#e5b713'
				},
				{
					label: 'BIDS ON AWARDS PROJECT',
					data: [15,25,29],
					backgroundColor: '#189fee'
				},
				{
					label: 'BIDS PlACED',
					data: [5,20],
					backgroundColor: '#174187'
				}
			]
		},

		options: {
			cutoutPercentage: 10,
			animation: {
				animateScale: true
			},
			scales: {
		        xAxes: [{
		            stacked: true 
		        }],
		        yAxes: [{
		            stacked: true 
		        }]
		    }
		}
	});