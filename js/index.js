'use strict';

jQuery(window).on('scroll', function(){
      if(jQuery(window).scrollTop()){
        jQuery('header').addClass('fixed-it')
      }
      else{
        jQuery('header').removeClass('fixed-it')
      }
    })

    $(".navbar-toggler").on('click', function() {
        $(".content-pt").toggleClass("blur-it");
        $("header").toggleClass("opacity-it");
    });





/* Optional: Add active class to the current button (highlight it) */
var container = document.getElementsByClassName("buttons-container");
var btns = document.getElementsByClassName("view-btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    if (current.length > 0) { 
    current[0].className = current[0].className.replace(" active", "");
  }
  this.className += " active";
  });
}


$(window).on('load', function() {

  $(".testimonial-slider").slick({
    dots: false,
    arrows:true,
    infinite: true,
    autoplay:true,
    centerMode:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: false,
    fade: true,
    cssEase: 'linear'
  });

});

// $(window).on('load', function() {

//   $(".weeding-slider").slick({
//     dots: false,
//     arrows:true,
//     infinite: true,
//     autoplay:true,
//     centerMode:true,
//     slidesToShow: 4,
//     slidesToScroll: 4,
//     variableWidth: true,
//     fade: true,
//     cssEase: 'linear'
//   });

// });

// $(window).on('load', function() {

//   $(".center-details-slider").slick({
//     dots: true,
//     arrows:true,
//     infinite: true,
//     autoplay:true,
//     centerMode:true,
//     slidesToShow: 4,
//     slidesToScroll: 4,
//     variableWidth: true,
//     fade: true,
//     cssEase: 'linear'
//   });

// });

 $(function () {
   var bindDatePicker = function() {
    $(".date").datetimepicker({
        format:'YYYY-MM-DD',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
      }
    }).find('input:first').on("blur",function () {
      // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
      // update the format if it's yyyy-mm-dd
      var date = parseDate($(this).val());

      if (! isValidDate(date)) {
        //create date based on momentjs (we have that)
        date = moment().format('DD-MM-YYYY');
      }

      $(this).val(date);
    });
  }
   
   var isValidDate = function(value, format) {
    format = format || true;
    // lets parse the date to the best of our knowledge
    if (format) {
      value = parseDate(value);
    }

    var timestamp = Date.parse(value);

    return isNaN(timestamp) == true;
   }
   
   var parseDate = function(value) {
    var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
    if (m)
      value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

    return value;
   }
   
   bindDatePicker();

 });



 // dynamic replicate field
    // $('.add-center-btn').on('click', function (e) {
    //   e.preventDefault();
    //   var proposal =  '<div class="container">'+
    //   '<div class="row">'+
    //   '<div class="col-lg-12 col-md-12 col-sm-12">'+
    //       '<div class="hotelier-sign-up-form top-form">'+
    //       '<form action="#" class="hotelier-form top-form">' +
    //             '<div class="left-side">' +
    //               '<label for="type">Type</label>' +
    //             '<select name="" id="type">' +
    //             '<option value="">Hall</option>' +
    //               '</select>' +
    //               '<div class="date-picker">' +
    //             '<div class="input-group date" data-provide="datepicker">'+
    //             '<input type="text" class="form-control" placeholder="From">'+
    //             '<div class="input-group-addon">'+
    //             '<span class="glyphicon glyphicon-th">'+
    //             '</span>'+
    //             '</div>'+
    //             '</div>'+
    //             '</div>'+
    //             '<div class="right-side">'+
    //             '<label for="price">'+
    //             'Price'+'</label>'+
    //             '<input type="text">'+
    //             '<div class="date-picker">'+
    //             '<div class="input-group date" data-provide="datepicker">'+
    //             '<input type="text" class="form-control" placeholder="To">'+
    //             '<div class="input-group-addon">'+'<span class="glyphicon glyphicon-th">'+'</span>'+
    //             '</div>'+'</div>'+'</div>'+'</div>'+
    //             '</form>'+
    //             '<div class="profile-area">'+
    //             '<img src="img/weeding-website/profile2.jpg" alt="">'+
    //             '<span class="close">'+'<i class="fas fa-times"></i>'+'</span>'+'</div>'+
    //             '<div class="profile-area">'+
    //             '<img src="img/weeding-website/profile3.jpg" alt="">'+
    //             '<span class="close">'+'<i class="fas fa-times"></i>'+'</span>'+'</div>'+
    //             '<div class="file-upload">'+
    //             '<label for="upload" class="file-upload__label">'+'<i class="fas fa-plus">'+
    //             '</i>'+'</label>'+'<input id="upload" class="file-upload__input" type="file" name="file-upload">'+'</div>'
    //             '</div>'+'</div>'+'</div>'
    //             '</div>';
    //   $('.hotelier-sign-up-form').append(proposal);
    // });