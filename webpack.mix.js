let mix = require('laravel-mix');

mix.sass('src/scss/main.scss', 'dist/css')
	.js('src/js/app.js', 'dist/js');